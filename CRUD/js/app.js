document.addEventListener("DOMContentLoaded", function(event) {

const apiUrl="http://jsonplaceholder.typicode.com/comments";
const userList=document.querySelector(".user_tbody");
const addBtn=document.querySelector("#addUsr");

const nameInput=document.querySelector(".get_name");
const emailInput=document.querySelector(".get_mail");
const bodyInput=document.querySelector(".get_body");
const error=document.querySelector(".error");


//event on button -> add to database user-> star function
addBtn.addEventListener("click",function(event) {
  event.preventDefault();
  //check all inputs, they can't by empty
  if (nameInput.value==="" || emailInput.value==="" || bodyInput.value==="" ) {
        error.innerText="Wypełnij wszystkie pola!";
  }
  else if (emailInput.value.indexOf("@")===-1 ) {
      error.innerText="Brakuje znaku @ w mailu !";
  }

  else{
        error.innerText='';

// take value of inputs
  let nameFromInput = nameInput.value;
  let emailFromInput = emailInput.value;
  let bodyFromInput = bodyInput.value;

  //create new object
  var newUser = {
    name:nameFromInput,
    email:emailFromInput,
    body:bodyFromInput,
  }
    $.ajax({
      type:"POST",
      url:apiUrl,
      data:newUser,
      dataType:"json"
    }).done(function (response) {
        renderUsers(response);
    }).fail(function (error) {
        console.log("error form POST");
  });

}//end of else
});
//event on button -> add to database user-> end of function



//function renderUsers-> start function

function renderUsers(user) {

  let newTrElement= document.createElement("tr");
  //create new tr
  //create 2 buttons
  let removeBtn = document.createElement("button");
  let editBtn = document.createElement("button");


  removeBtn.classList.add("delete");//add "delete" class
  removeBtn.innerText="Delete";
  editBtn.classList.add("edit");//add "edit" class
  editBtn.innerText="Edit";

  let tdBtn1 =document.createElement("td");
  let tdBtn2 =document.createElement("td");


  // nick.classList.add("nick");
  //create new td with name user
  let name =  document.createElement("td");
  name.innerText=user.name;
  name.classList.add("name");
  //create new td with email user
  let email =  document.createElement("td");
  email.innerText=user.email;
  email.classList.add("email");
  //create new td with body user
  let body =  document.createElement("td");
  body.innerText=user.body;
  body.classList.add("body");

  newTrElement.dataset.id=user.id;
  //add id to new tr
  name=newTrElement.appendChild(name);
  email=newTrElement.appendChild(email);
  body=newTrElement.appendChild(body);

  //add 3xtd to tr

  editBtn=tdBtn1.appendChild(editBtn);
  removeBtn=tdBtn2.appendChild(removeBtn);
  tdBtn1=newTrElement.appendChild(tdBtn1);
  tdBtn2=newTrElement.appendChild(tdBtn2);

  newTrElement=userList.appendChild(newTrElement);
  // add td to tr


  /// event on removeBtn to delete clicked row.
  removeBtn.addEventListener("click",function (event) {
    let trToDel = this.parentElement.parentElement;
    let clickedDelId= trToDel.getAttribute('data-id');
    $.ajax({
      type:"DELETE",
      url:apiUrl+'/'+clickedDelId,//go to clicked row
      dataType:"json"
    }).done (function (response) {
      // remove table row for table
      trToDel.parentNode.removeChild(trToDel);
    }).fail(function (error) {
      console.log("error from DELETE");
    });
  });//end of event removeBtn to delete

}//end of click


  /////////////////////////////////
  //PAGINACJA///

  let pageNr=0;
  //use setTimeout to delay pagination
setTimeout(function () {

  const howManyPages = 100;// here take how many tr must be in page
  let tr=userList.children;
  const back = document.querySelector(".back");
  const next = document.querySelector(".next");
  const pages = document.querySelector(".pages");

  let count = howManyPages - 1 ;
  let mincount = 0;


//show first page
  for (let i = 0; i < tr.length; i++) {
    if (i>count)  {
      tr[i].style.display='none';
    }else if (i<mincount) {
      tr[i].style.display='none';
    }
  }

//event on next button
  next.addEventListener("click",function () {
    back.disabled=false;
    count = count + howManyPages;
    mincount = mincount +howManyPages ;
    for (let i = 0; i < tr.length; i++) {
      console.log(count);
      if (count >=tr.length) {
        next.disabled=true;
      }
      else if (i>count)  {
        tr[i].style.display='none';
      }else if (i<mincount) {
        tr[i].style.display='none';
      }
      else{
        tr[i].style.display='';
      }
    }
  });

// event on back button
  back.addEventListener("click",function () {
    next.disabled=false;
    count = count - howManyPages;
    mincount = mincount - howManyPages ;
    for (let i = 0; i < tr.length; i++) {
      if (mincount <=0) {
        back.disabled=true;
      }else if (i>count)  {
        tr[i].style.display='none';
      }else if (i<mincount) {
        tr[i].style.display='none';
      }
      else{
        tr[i].style.display='';
      }
    }
  });

//create a spans to jump throught the pages
  for (var i = 0; i < tr.length; i++) {
    let result = (i/howManyPages)+1;
    let strongResult = result.toString();

    if (strongResult.indexOf('.') === -1 ) {
      let page = document.createElement("span");
      page.classList.add("pageSpan");
      pageNr++;
      textToPage = pageNr;
      page.innerText=textToPage;
      pages.appendChild(page);
    }
  }

  let spans= document.querySelectorAll(".pageSpan");

//setting the min and max tr to show
  [...spans].forEach(function (span) {
    span.addEventListener("click",function (event) {
      count = howManyPages;
      let clickSpan = parseInt(span.innerText);
      count = count * clickSpan;
      let mincount = count-howManyPages;

      console.log(count,"count");
      console.log(mincount,"min");

      for (let i = 0; i < tr.length; i++) {
        if (i>=count)  {
          tr[i].style.display='none';
        }else if (i<mincount) {
          tr[i].style.display='none';
        }
        else{
          tr[i].style.display='';
        }
      }
    });
  });
},200);//end of pagination

////////////////////////////////////////////////////





$("body").on("click",".edit", function (){
    let $trToEdit = $(this).parent().parent();
    let $clickedEditId = $trToEdit.data("id");

    // in new val I take new text from classes
    let $oldName = $trToEdit.find(".name").text();
    let $oldEmail = $trToEdit.find(".email").text();
    let $oldBody = $trToEdit.find(".body").text();

    $trToEdit.addClass("editable");
    $trToEdit.children().hide();

    //create new td
    $tdToIn = $("<td>");
    $tdToIn2 = $("<td>");
    $tdToIn3 = $("<td>");

    //create new inputs and take them value
    let $nameInput = $("<input>");
    $nameInput.val($oldName);

    let $emailInput = $("<input>");
    $emailInput.val($oldEmail);

    let $bodyInput = $("<input>");
    $bodyInput.val($oldBody);

    let $submitButton = $("<button>");
    $submitButton.text("Save");


    //appent all things wht i create
    $tdToIn.append($nameInput);
    $tdToIn2.append($emailInput);
    $tdToIn3.append($bodyInput);

    $trToEdit.append($tdToIn);
    $trToEdit.append($tdToIn2);
    $trToEdit.append($tdToIn3);
    $trToEdit.append($submitButton);

    //start click edit btn
    $submitButton.on("click",function() {
      let nameFromInputSave = $nameInput.val();
      let emailFromInputSave = $emailInput.val();
      let bodyFromInputSave = $bodyInput.val();

    if ($nameInput.val()==="" || $emailInput.val()==="" || $bodyInput.val()==="" ) {
          $(".error").text("Wypełnij wszystkie pola!");
      }else if ($emailInput.val().indexOf("@")===-1 ) {
        $(".error").text("Brakuje znaku @ w mailu !!");
      }else{



      //create new object
      let editUser = {
        name: nameFromInputSave,
        email: emailFromInputSave,
        body: bodyFromInputSave
      };

      $.ajax({
        type:"PUT",
        url: apiUrl + '/' + $clickedEditId,
        data: editUser,
        dataType: 'json'
      }).done(function (response) {
        //show TR ane remove class edit
        $trToEdit.children().show();
        $trToEdit.removeClass("editable");

        //remove all what i create in click edit
        $tdToIn.remove();
        $tdToIn2.remove();
        $tdToIn3.remove();
        $nameInput.remove();
        $emailInput.remove();
        $bodyInput.remove();
        $submitButton.remove();

        //add text to this classes
        $trToEdit.find(".name").text(response.name);
        $trToEdit.find(".email").text(response.email);
        $trToEdit.find(".body").text(response.body);

      }).fail(function (error) {
        console.log("error form PUT edit");
      });
    }

    });//end of submit btn
  }); // end of edit btn





function loadUser() {

  $.ajax({
    type:"GET",
    url:apiUrl,
    dataType:"json"
  }).done(function (response) {
    for (var i = 0; i < response.length; i++) {
      renderUsers(response[i]);
    }
  }).fail(function (error) {
    console.log("error from loadUser");
  })
}
loadUser();

















});
