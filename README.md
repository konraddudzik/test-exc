# Zadanko testowe
Chętnie zobaczę jak piszesz i jakie masz nawyki :)

## Dobór technologi zależy w 100% od Ciebie.

----
####Odwzoruj w jak najdokładniejszy sposób zaprezentowaną stronę(img/site.png).

Dla chętnych:

* Dodatkowo dodaj elementy RWD
* MobileFirst :]
* Bem

###Stwórz CRUD’a dzięki któremu będzie można zarządzać zasobami: 
```http://jsonplaceholder.typicode.com/comments```

###Feature’y:  tworzenie/edycja/listowanie/usuwanie

* Dodatkowo możesz dodać paginację ```http://jsonplaceholder.typicode.com/comments?_page=7&_limit=2```
* Dodatkowo możesz dodać filtrowanie po wybranych kolumnach
